

<?php

require "lib/dbCon.php";
require "lib/chitiettin.php";

if (isset($_GET["idTin"])) {
    $idTin = $_GET["idTin"];
    settype($idTin, "int");
    DemLanXemTin($idTin);
} else {
    $idTin = 1;
    DemLanXemTin($idTin);
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head prefix="og: http://ogp.me/ns# fb:http://ogp.me/ns/fb# article:http://ogp.me/ns/article#">
    <link rel="shortcut icon" href="/favicon.ico"/>
    <link rel="amphtml" href="http://www.phapluatplus.vn/news/4617.amp">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="content-language" content="vi"/>
    <script type="text/javascript" src="lib/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="comment_insert.js?t=<?php echo time(); ?>"></script>
    <title>VTC News: Tin tức 24h trong ngày - Đọc báo điện tử mới nhất hôm nay</title>
    <link href="css/main.css" rel="stylesheet" type="text/css"/>
    <link href="css/exp.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="lib/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="comment_insert.js?t=<?php echo time(); ?>"></script>
    <script >

        $(document).ready(function () {
            var idBar= $(this).attr('href');
            var fields = idBar.split('idTin=');

            var idTin = fields[1];
            console.log("da vao ẫ ");
            $.ajax({
             url : "block2/comment_box.php",
             type : "get",
             dataType:"text",
             data : {
             'idTin='+idTin
             },
             success : function (result){
             $('.comment-holder-ul').html(result);
             }
             });

             $.get("block2/comment_box.php",function (data) {
             $(".comment-holder-ul").html(data);
             });
        });
        console.log("chay vo script");
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-68148973-1', 'auto');
        ga('send', 'pageview');

    </script>

    <meta name="eclick_verify" content="dFFUXlYXLRcYGVlEahMECBMHMAIYGQ8eNk0aBw=="/>
    <link rel="stylesheet" type="text/css" href="/lib/explus/explus.css?v=0"/>
    <title>''Mổ xẻ'' bài thuốc nam chữa khỏi bệnh trĩ bằng Đông y ở Việt Nam</title>
    <link href="css/index2.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="admin/assets/css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<?php
/*require "block/menu_left.php";*/
?>
<div class="wrapper">
    <div id="header-sticky-wrapper" class="sticky-wrapper">
        <div id="header">
            <?php
            require "block/header_menu.php";
            ?>
        </div>
    </div>
    <div class="news_hot pkg">

        <?php
        require "block/maraquee_hot.php";
        ?>

        <ul class="fr list_weather">
            <li style="float: right;"><a title="Sự kiện nóng trong ngày" href="index.php">SỰ KIỆN HOT</a></li>
            <li style="float: right; margin: 0;">HOTLINE: MB - 01255911911, MN - 0911848186</li>
        </ul>

        <div style="clear: both;"></div>

    </div>


    <div class="details">
        <div class="pull-left" style="width: calc(100% - 300px - 20px);">
            <article class="pull-left" style="width: calc(100% - 160px - 20px) ;height: 500px">

                <?php
                require "block2/content_main.php";
                require "block2/comment.php";
                ?>
            </article>
            <div class="clearfix"></div>

            <?php
            require "block2/tin_lienquan_left.php";
            ?>
            <div class="clearfix"></div>
            <?php
            require "block2/tin_lienquan_bottom.php";


            ?>

        </div>
        <div class="col-fixed-300 pull-right">
            <div style="text-align:center;margin-top:0px;margin-bottom:0px;">
                <div style="width:300px;height:250px;">
                    <script type="text/javascript" src="//media1.admicro.vn/ads_codes/ads_box_38749.ads"></script>
                </div>
            </div>
            <div class="space20"></div>
            <style>
                .box-paper {
                    position: relative;
                }

            </style>
            <?php
            require "block2/qcao_left_2.php";
            ?>
            <?php
            require "block2/doc_nhieu.php";
            ?>
            <div style="display:inline-block;width:300px;margin-left:0px;margin-right:0px;"></div>
            <?php
            require "block2/tin_rao_vat.php";
            ?>
            <div style="text-align:center;margin-top:8px;margin-bottom:0px;">
                <div style="margin-top:0px;width:300px;height:600px;">
                    <script type="text/javascript" src="upload/quangcao/qc1.jpg"></script>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>


</div>
</main>
<script type="text/javascript">
    ga('send', 'event', 'category_17', 'Explorer', 'Giáo dục - Sức khỏe');
</script>
<div class="scroll-top-wrapper ">
         <span class="scroll-top-inner">
         <i class="fa fa-2x fa-arrow-circle-up"></i>
         </span>
</div>

</body>

</html>

