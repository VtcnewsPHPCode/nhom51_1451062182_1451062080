<style type="text/css">
    .message {
        color: red;
    }
</style>
<?php
require "../lib/trangquantri.php";

?>

<?php

if (isset($_POST["btnThem"])) {
    $error = array();
    if (empty($_POST['Ten'])) {
        $error[] = 'Ten';
    } else {
        $Ten = $_POST["Ten"];

        $Ten_KhongDau = changeTitle($Ten);
    }


    if (empty($_POST['ThuTu'])) {
        $error[] = 'ThuTu';
    } else {
        $ThuTu = $_POST["ThuTu"];
        settype($ThuTu, "int");
    }


    if (empty($_POST['AnHien'])) {
        $AnHien="0";
    } else {
        $AnHien = $_POST["AnHien"];
        settype($AnHien, "int");
    }
    if (empty($error)) {
        $sql = "INSERT INTO loaitin VALUES (null,'$Ten','$Ten_KhongDau',$ThuTu,$AnHien )";

        $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
        mysqli_set_charset($link, "UTF8");
        mysqli_query($link, $sql);
        echo($sql);
        header("location:../admin/dashboard.php?p=quanlyloaitin");

    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


</head>
<body>

<div class="wrapper">
    <?php
    require "blockadmin/sidebar.php";
    ?>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="height:100%;">


        <div id="ja-mainbody" class="clearfix">

            <form method="POST" name="edit_course">
                <table class="table-form-edit" align="center" bgcolor="#FFFFFF">
                    <div class="form-group">
                        <td colspan="2" align="left"><strong>Thêm mới loại tin</strong>
                    </div>

                    <div class="form-group">
                        Tên loại tin
                        <input class="form-control" name="Ten" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('Ten', $error)) {
                            echo "<p class='message'>Ten không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Thứ tự
                        <input class="form-control" name="ThuTu" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('ThuTu', $error)) {
                            echo "<p class='message'>ThuTu không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">

                        Trạng thái


                        <input name="AnHien" value="0" checked=checked type="radio"> Ẩn
                        <input name="AnHien" value="1" type="radio">Hiện
                        <?php
                        if (isset($error) && in_array('AnHien', $error)) {
                            echo "<p class='message'>Trạng thái không hợp lệ</p>";
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        <input class="form-control btn btn-danger" type="submit" value="Lưu lại" name="btnThem">
                        <input class="form-control btn btn-danger" type="reset" value="Reset">
                    </div>
                </table>
            </form>


        </div>


    </div>

    <footer class="footer" style="background: #f4f3ef;">
        <div class="container-fluid">
            <nav class="pull-left">

            </nav>
            <div style="margin-bottom: 12px;" class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script>
                , made with <i class="fa fa-heart heart"></i> by <a
                        href="http://localhost/nhom51_1451062182_1451062080/source/index.php">Creative Team
                    51_1451062182_1451062080</a>

            </div>
        </div>
    </footer>
</div>
</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="assets/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>


</html>
