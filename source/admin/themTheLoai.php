<style type="text/css">
    .message {
        color: red;
    }
</style>
<?php
require "../lib/trangquantri.php";

?>

<?php

if (isset($_POST["btnThem"])) {
    $error = array();
    if (empty($_POST['TenTL'])) {
        $error[] = 'TenTL';
    } else {
        $TenTL = $_POST["TenTL"];
        $TenTL_KhongDau = changeTitle($TenTL);
    }
    if (empty($_POST['ThuTu'])) {
        $error[] = 'ThuTu';
    } else {
        $ThuTu = $_POST["ThuTu"];
        settype($ThuTu, "int");
    }

    if (empty($_POST['AnHien'])) {
        $AnHien = "0";
    } else {
        $AnHien = $_POST["AnHien"];
        settype($AnHien, "int");
    }

    if (empty($_POST['idLT'])) {
        $error[] = 'idLT';
    } else {
        $idLT = $_POST["idLT"];
        settype($idLT, "int");
    }

    if (empty($error)) {

        $sql = "INSERT INTO theloai VALUES (null,'$TenTL','$TenTL_KhongDau',$ThuTu,$AnHien,$idLT )";

        $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
        mysqli_set_charset($link, "UTF8");
        mysqli_query($link, $sql);
        echo $sql;
        header("location:../admin/dashboard.php?p=quanlytheloai");
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


</head>
<body>

<div class="wrapper">
    <?php
    require "blockadmin/sidebar.php";
    ?>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="background: #76FF03;height:100%;">


        <div id="ja-mainbody" class="clearfix">

            <div class="title-module">Thêm mới thể loại</div>
            <form method="POST" name="edit_course">
                <table class="table-form-edit" align="center" bgcolor="#FFFFFF">
                    <div class="form-group">
                        <td colspan="2" align="left"><strong>Thêm mới thể loại</strong>
                    </div>

                    <div class="form-group">
                        Tên thể loại
                        <input class="form-control" name="TenTL" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('TenTL', $error)) {
                            echo "<p class='message'>TenTL không hợp lệ</p>";
                        }
                        ?>
                    </div>

                    <div class="form-group">
                        Thứ tự
                        <input class="form-control" name="ThuTu" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('ThuTu', $error)) {
                            echo "<p class='message'>ThuTu không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        ID Loại tin

                        <select class="form-control" name="idLT">
                            <?php
                            $theloai = DanhSachLoaiTin();
                            while ($row_theloai = mysqli_fetch_assoc($theloai)) {
                                ?>
                                <option value="<?php echo $row_theloai['idLT']; ?> "
                                        selected><?php echo $row_theloai['Ten']; ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <div class="form-group">

                        Trạng thái

                        <input name="AnHien" value="0" checked=checked type="radio"> Ẩn
                        <input name="AnHien" value="1" type="radio">Hiện

                    </div>

                    <div class="form-group">
                        <input class="form-control btn btn-danger" type="submit" value="Lưu lại" name="btnThem">
                        <input class="form-control btn btn-danger" type="reset" value="Reset">
                    </div>
                </table>
            </form>


        </div>


    </div>

    <footer class="footer" style="background: #f4f3ef;">
        <div class="container-fluid">
            <nav class="pull-left">

            </nav>
            <div style="margin-bottom: 12px;" class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script>
                , made with <i class="fa fa-heart heart"></i> by <a
                        href="http://localhost/nhom51_1451062182_1451062080/source/index.php">Creative Team
                    51_1451062182_1451062080</a>

            </div>
        </div>
    </footer>
</div>
</body>


</html>
