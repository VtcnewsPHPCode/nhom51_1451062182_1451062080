<?php
require "../lib/trangquantri.php";


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


</head>
<body>

<div class="wrapper">


    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:100%;">


        <div id="ja-mainbody" class="clearfix">


        </div>

        <div style="width: 100%;" class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Danh sách loại tin</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                    <th>idLT</th>
                                    <th>TenTL</th>
                                    <th>TenTL_KhongDau</th>
                                    <th>ThuTu</th>
                                    <th>AnHien</th>
                                    <th><a href="themloaitin.php">Thêm</a></th>
                                    </thead>
                                    <tbody>
                                    <?php

                                    $theloai = DanhSachLoaiTin();
                                    while ($row_theloai = mysqli_fetch_assoc($theloai)) {
                                        ob_start();
                                        ?>

                                        <tr>
                                            <td>{idLT}</td>
                                            <td>{Ten}</td>
                                            <td>{Ten_KhongDau}</td>
                                            <td>{ThuTu}</td>
                                            <td>{AnHien}</td>
                                            <td><a href="sualoaitin.php?idLT={idLT}">Sửa</a>-<a
                                                        onclick="return confirm('Bạn có chắc chắn muốn xóa hay không')"
                                                        href="xoaloaitin.php?idLT={idLT}">Xóa</a></td>

                                        </tr>
                                        <?php
                                        //ob_start  vaf ob_get_clean maf  bao html nao thi  html nay chinh la chuoi $s
                                        //có tac dung bien html thanh 1 chuoi nao do
                                        $s = ob_get_clean();
                                        // $s=str_replace(search, replace, subject)
                                        //search tu can tim kiem , subject tim kiem trong chuoi html
                                        $s = str_replace("{idLT}", $row_theloai["idLT"], $s);
                                        $s = str_replace("{Ten}", $row_theloai["Ten"], $s);
                                        $s = str_replace("{Ten_KhongDau}", $row_theloai["Ten_KhongDau"], $s);
                                        $s = str_replace("{ThuTu}", $row_theloai["ThuTu"], $s);
                                        $s = str_replace("{AnHien}", $row_theloai["AnHien"], $s);

                                        echo "$s";
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>

    <footer class="footer" style="background: #f4f3ef;">
        <div class="container-fluid">
            <nav class="pull-left">

            </nav>
            <div style="margin-bottom: 12px;" class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script>
                , made with <i class="fa fa-heart heart"></i> by <a
                        href="http://localhost/nhom51_1451062182_1451062080/source/index.php">Creative Team
                    51_1451062182_1451062080</a>

            </div>
        </div>
    </footer>
</div>
</body>


</html>
