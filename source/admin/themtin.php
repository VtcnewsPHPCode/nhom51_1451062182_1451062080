<style type="text/css">
    .message {
        color: red;
    }
</style>
<?php
require "../lib/trangquantri.php";

?>
<?php

if (isset($_POST["btnThem"])) {
    $error = array();
    if (empty($_POST['TieuDe'])) {
        $error[] = 'TieuDe';
    } else {
        $TieuDe = $_POST["TieuDe"];
        $TieuDe_KhongDau = changeTitle($TieuDe);
    }

    if (empty($_POST['TomTat'])) {
        $error[] = 'TomTat';
    } else {
        $TomTat = $_POST["TomTat"];
    }


    if (empty($_POST['Ngay'])) {
        $error[] = 'Ngay';
    } else {
        $Ngay = $_POST["Ngay"];
    }

    if (empty($_POST['idUser'])) {
        $error[] = 'idUser';
    } else {
        $idUser = $_POST["idUser"];
        settype($idUser, "int");
    }


    if (empty($_POST['Content'])) {
        $error[] = 'Content';
    } else {
        $Content = $_POST["Content"];
    }

    if (empty($_POST['idLT'])) {
        $error[] = 'idLT';
    } else {
        $idLT = $_POST["idLT"];
        settype($idLT, "int");
    }


    if (empty($_POST['idTL'])) {
        $error[] = 'idTL';
    } else {
        $idTL = $_POST["idTL"];
        settype($idTL, "int");
    }
    if (empty($_POST['SoLanXem'])) {
        $error[] = 'SoLanXem';
    } else {
        $SoLanXem = $_POST["SoLanXem"];
        settype($SoLanXem, "int");
    }

    if (empty($_POST['TinNoiBat'])) {
        $AnHien = "0";
    } else {

        $TinNoiBat = $_POST["TinNoiBat"];
    }

    if (empty($_POST['AnHien'])) {
        $AnHien = "0";
    } else {
        $AnHien = $_POST["AnHien"];
        settype($AnHien, "int");
    }
    if (empty($error)) {
        if ($_FILES['urlHinh']['type'] == "image/jpeg"
            || $_FILES['urlHinh']['type'] == "image/png"
            || $_FILES['urlHinh']['type'] == "image/jpg"
        ) {
            $urlHinh = $_FILES['urlHinh']['name'];
            move_uploaded_file($_FILES["urlHinh"]["tmp_name"], "../upload/tintuc/" . $urlHinh);
        }
        $sql = "INSERT INTO tin VALUES (null,'$TieuDe','$TieuDe_KhongDau','$TomTat','$urlHinh','$Ngay', '$idUser','$Content','$idLT','$idTL',
  '$SoLanXem','$TinNoiBat','$AnHien')";

        $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
        mysqli_set_charset($link, "UTF8");
        mysqli_query($link, $sql);
        header("location:../admin/dashboard.php?p=quanlytin");
    }
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


    <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="ckfinder/ckfinder.js" type="text/javascript"></script>

</head>
<body>

<div class="wrapper">
    <?php
    require "blockadmin/sidebar.php";
    ?>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="height:100%;">


        <div id="ja-mainbody" class="clearfix">

            <div class="title-module">Thêm mới tin</div>
            <form method="POST" name="edit_course" enctype="multipart/form-data">
                <table class="table-form-edit" align="center" bgcolor="#FFFFFF">
                    <div class="form-group">
                        <td colspan="2" align="left"><strong>Thêm mới tin</strong>
                    </div>

                    <div class="form-group">
                        Tiêu đề
                        <textarea class="form-control" name="TieuDe" id="TieuDe" cols="45" rows="2"></textarea>
                        <?php
                        if (isset($error) && in_array("TieuDe", $error)) {
                            echo "<p class='message'>TieuDe không hợp lệ</p>";
                        }
                        ?>

                    </div>
                    <div class="form-group">
                        Tóm tắt

                        <textarea class="form-control" name="TomTat" id="TomTat" cols="45" rows="5"></textarea>
                        <?php
                        if (isset($error) && in_array("TomTat", $error)) {
                            echo "<p class='message'>TomTat không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        url hình
                        <input class="form-control" name="urlHinh" type="file" value="" size="80%">
                        <?php
                        if (isset($error) && in_array("urlHinh", $error)) {
                            echo "<p class='message'>urlHinh không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Ngày
                        <input class="form-control" name="Ngay" type="date" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('Ngay', $error)) {
                            echo "<p class='message'>Ngay không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        idUser
                        <input class="form-control" name="idUser" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('idUser', $error)) {
                            echo "<p class='message'>idUser không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Nội dung
                        <textarea class="form-control" name="Content" id="Content" cols="45" rows="5"></textarea>
                        <script type="text/javascript">
                            var editor = CKEDITOR.replace('Content', {
                                uiColor: '#9AB8F3',
                                language: 'vi',
                                skin: 'v2',
                                filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?Type=Images',
                                filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?Type=Flash',
                                filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',


                                toolbar: [
                                    ['Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
                                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print'],
                                    ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
                                    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
                                    ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                                    ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                                    ['Link', 'Unlink', 'Anchor'],
                                    ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                                    ['Styles', 'Format', 'Font', 'FontSize'],
                                    ['TextColor', 'BGColor'],
                                    ['Maximize', 'ShowBlocks', '-', 'About']
                                ]
                            });
                        </script>
                        <?php
                        if (isset($error) && in_array('Content', $error)) {
                            echo "<p class='message'>Content không hợp lệ</p>";
                        }
                        ?>

                    </div>
                    <div class="form-group">
                        ID Loại tin

                        <select class="form-control" name="idLT">
                            <?php
                            $theloai = DanhSachLoaiTin();
                            while ($row_theloai = mysqli_fetch_assoc($theloai)) {
                                ?>
                                <option value="<?php echo $row_theloai['idLT']; ?> "><?php echo $row_theloai['Ten']; ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <div class="form-group">
                        ID Thể loại

                        <select class="form-control" name="idTL">
                            <?php
                            $theloai = DanhSachTheLoai();
                            while ($row_theloai = mysqli_fetch_assoc($theloai)) {
                                ?>
                                <option value="<?php echo $row_theloai['idTL']; ?> "><?php echo $row_theloai['TenTL']; ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <div class="form-group">
                        Số Lần Xem
                        <input name="SoLanXem" class="form-control" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('SoLanXem', $error)) {
                            echo "<p class='message'>SoLanXem không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Tin nổi bật
                        <input name="TinNoiBat" value="0" checked=checked type="radio"> Tin bình thường
                        <input name="TinNoiBat" value="1" type="radio">Tin nổi bật
                    </div>

                    <div class="form-group">

                        Ẩn Hiện


                        <input name="AnHien" value="0" checked=checked type="radio"> Ẩn
                        <input name="AnHien" value="1" type="radio">Hiện

                    </div>

                    <div class="form-group">

                        <input class="form-control btn btn-danger" type="submit" value="Lưu lại" name="btnThem">
                        <input class="form-control btn btn-danger" type="reset" value="Reset">
                    </div>
                </table>
            </form>


        </div>


    </div>




</div>
</body>


</html>
