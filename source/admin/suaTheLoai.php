<style type="text/css">
    .message {
        color: red;
    }
</style>
<?php
require "../lib/trangquantri.php";
?>
<?php
ob_start();
/*if(!isset($_SESSION["idUser"]) && $_SESSION["idGroup"]!=1)
{
header("location:../index.php");

}*/
?>
<?php

$idTL = $_GET["idTL"];
settype($idTL, "int");
$row_Theloai = ChiTietTheLoai($idTL);

?>

<?php

if (isset($_POST["btnSua"])) {
    $error = array();
    if (empty($_POST['TenTL'])) {
        $error[] = 'TenTL';
    } else {
        $TenTL = $_POST["TenTL"];
        $TenTL_KhongDau = changeTitle($TenTL);
    }
    if (empty($_POST['ThuTu'])) {
        $error[] = 'ThuTu';
    } else {
        $ThuTu = $_POST["ThuTu"];
        settype($ThuTu, "int");
    }

    if (empty($_POST['AnHien'])) {
        $error[] = 'AnHien';
        $AnHien = "0";

    } else {
        $AnHien = $_POST["AnHien"];
        settype($AnHien, "int");
    }

    if (empty($_POST['idLT'])) {
        $error[] = 'idLT';
    } else {
        $idLT = $_POST["idLT"];
        settype($idLT, "int");
    }
    if (empty($error)) {

        $sql = "UPDATE theloai SET
  TenTL='$TenTL',
  TenTL_KhongDau='$TenTL_KhongDau',
  ThuTu='$ThuTu',
  AnHien='$AnHien',
  idLT='$idLT'
  WHERE idTL='$idTL'
  ";

        $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
        mysqli_set_charset($link, "UTF8");
        mysqli_query($link, $sql);
        echo($sql);

        header("location:../admin/dashboard.php?p=quanlytheloai");
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


</head>
<body>

<div class="wrapper">
    <?php
    require "blockadmin/sidebar.php";
    ?>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="height:100%;">


        <div id="ja-mainbody" class="clearfix">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="height:1000px;">
                <form method="POST" name="edit_course">
                    <table class="table-form-edit" align="center">
                        <div class="form-group">
                            <td colspan="10" align="left"><strong>Sửa thể loại</strong>
                        </div>

                        <div class="form-group">
                            <p><label>Tên thể loại</label></p>
                            <input class="form-control" name="TenTL" type="<?php echo $row_Theloai["TenTL"]; ?>"
                                   value="<?php echo $row_Theloai["TenTL"]; ?>" size="40">
                            <?php
                            if (isset($error) && in_array('TenTL', $error)) {
                                echo "<p class='message'>TenTL không hợp lệ</p>";
                            }
                            ?>
                        </div>
                        <div style="margin-top:10px; " class="form-group">
                            <p><label>Thứ tự</label></p>
                            <input class="form-control" name="ThuTu" type="<?php echo $row_Theloai["ThuTu"]; ?>"
                                   value="<?php echo $row_Theloai["ThuTu"]; ?>" size="40">
                            <?php
                            if (isset($error) && in_array('ThuTu', $error)) {
                                echo "<p class='message'>ThuTu không hợp lệ</p>";
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <p><label>Trạng thái</label></p>
                            <input name="AnHien"
                                <?php if ($row_Theloai["AnHien"] == 0) echo "checked='checked'" ?> type="radio"> Ẩn
                            <input name="AnHien"
                                <?php if ($row_Theloai["AnHien"] == 1) echo "checked='checked'" ?> type="radio">Hiện
                        </div>

                        <div class="form-group">
                            <p><label>ID Loại tin</label></p>

                            <select class="form-control" name="idLT">
                                <?php
                                $theloai = DanhSachLoaiTin();
                                while ($row_theloai = mysqli_fetch_assoc($theloai)) {
                                    ?>
                                    <option <?php if ($row_theloai["idLT"] == $row_Theloai["idLT"]) echo "selected='selected'"; ?>
                                            value="<?php echo $row_theloai['idLT']; ?> "><?php echo $row_theloai['Ten']; ?></option>
                                    <?php
                                }
                                ?>

                            </select>

                        </div>

                        <div class="form-group">

                            <p><input class="form-control btn btn-success" type="submit" value="Lưu lại" name="btnSua">
                            </p><input class="form-control btn btn-danger" type="reset" value="Reset">
                        </div>
                    </table>
                </form>
            </div>

        </div>


    </div>

    <footer class="footer" style="background: #f4f3ef;">
        <div class="container-fluid">
            <nav class="pull-left">

            </nav>
            <div style="margin-bottom: 12px;" class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script>
                , made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>
            </div>
        </div>
    </footer>
</div>

</body>


</html>
