<?php
require "../lib/trangquantri.php";


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>


    <title>Paper Dashboard by Creative Tim</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>

</head>
<body>

<div class="wrapper">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:100%;">


        <div id="ja-mainbody" class="clearfix">


        </div>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Danh sách tin</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-striped">
                                    <thead>
                                    <th>idTin</th>
                                    <th>Tiêu đề</th>
                                    <th>Tên TL</th>
                                    <th>Số lần xem</th>
                                    <th><a href="themtin.php">Thêm</a></th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $tin = DanhSachTin();
                                    while ($row_tin = mysqli_fetch_assoc($tin)) {
                                        ob_start();
                                        ?>
                                        <tr>
                                            <td>idTin:{idTin} <br/> {Ngay}</td>
                                            <td><a href="suaTin.php?idTin={idTin}">{TieuDe}</a><br/><img
                                                        style="float: left;margin-right: 5px;"
                                                        src="../upload/tintuc/{urlHinh}" width="152" height="96">{TomTat}
                                            </td>
                                            <td>{TenTL}<br/>-<br/>{Ten}</td>
                                            <td>Số Lần xem:{SoLanXem}<br/>
                                                TinNoiBat{TinNoiBat} =Ẩn Hiện{AnHien}
                                            </td>
                                            <td><a href="suaTin.php?idTin={idTin}">Sửa</a>-<a
                                                        href="xoatin.php?idTin={idTin}">Xóa</a></td>

                                        </tr>
                                        <?php
                                        $s = ob_get_clean();
                                        $s = str_replace("{idTin}", $row_tin["idTin"], $s);
                                        $s = str_replace("{Ngay}", $row_tin["Ngay"], $s);
                                        $s = str_replace("{TieuDe}", $row_tin["TieuDe"], $s);
                                        $s = str_replace("{TomTat}", $row_tin["TomTat"], $s);
                                        $s = str_replace("{TenTL}", $row_tin["TenTL"], $s);
                                        $s = str_replace("{Ten}", $row_tin["Ten"], $s);
                                        $s = str_replace("{SoLanXem}", $row_tin["SoLanXem"], $s);
                                        $s = str_replace("{TinNoiBat}", $row_tin["TinNoiBat"], $s);
                                        $s = str_replace("{AnHien}", $row_tin["AnHien"], $s);
                                        $s = str_replace("{urlHinh}", $row_tin["urlHinh"], $s);

                                        echo "$s";
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        </tbody>
        </table>

    </div>
</div>
</div>

</div>
</div>
</div>


</div>

<footer class="footer" style="background: #f4f3ef;">
    <div class="container-fluid">
        <nav class="pull-left">

        </nav>
        <div style="margin-bottom: 12px;" class="copyright pull-right">
            &copy;
            <script>document.write(new Date().getFullYear())</script>
            , made with <i class="fa fa-heart heart"></i> by <a
                    href="http://localhost/nhom51_1451062182_1451062080/source/index.php">Creative Team
                51_1451062182_1451062080</a>

        </div>
    </div>
</footer>
</div>
</body>


</html>
