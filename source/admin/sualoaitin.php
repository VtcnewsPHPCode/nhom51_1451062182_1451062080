<style type="text/css">
    .message {
        color: red;
    }
</style>
<?php
require "../lib/trangquantri.php";
?>
<?php
ob_start();
/*if(!isset($_SESSION["idUser"]) && $_SESSION["idGroup"]!=1)
{
header("location:../index.php");

}*/
?>
<?php

$idLT = $_GET["idLT"];
settype($idLT, "int");
$row_Theloai = ChiTietLoaiTin($idLT);

?>

<?php

if (isset($_POST["btnSua"])) {
    $error = array();
    if (empty($_POST['Ten'])) {
        $error[] = 'Ten';
    } else {
        $Ten = $_POST["Ten"];

        $Ten_KhongDau = changeTitle($Ten);
    }


    if (empty($_POST['ThuTu'])) {
        $error[] = 'ThuTu';
    } else {
        $ThuTu = $_POST["ThuTu"];
        settype($ThuTu, "int");
    }


    if (empty($_POST['AnHien'])) {
        $error[] = 'AnHien';
    } else {
        $AnHien = $_POST["AnHien"];
        settype($AnHien, "int");
    }
    if (empty($error)) {
        $sql = "UPDATE loaitin SET
  Ten='$Ten',
  Ten_KhongDau='$Ten_KhongDau',
  ThuTu='$ThuTu',
  AnHien='$AnHien'
  WHERE idLT='$idLT'
  ";

        $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
        mysqli_set_charset($link, "UTF8");
        mysqli_query($link, $sql);

        header("location:../admin/dashboard.php?p=quanlyloaitin");
    }


}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


</head>
<body>

<div class="wrapper">
    <?php
    require "blockadmin/sidebar.php";
    ?>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="height:100%;">


        <div id="ja-mainbody" class="clearfix">

            <div class="title-module">Sửa loại tin</div>
            <form method="POST" name="edit_course">
                <table class="table-form-edit" align="center" bgcolor="#FFFFFF">
                    <div class="form-group">
                        <td colspan="2" align="left"><strong>Sửa loại tin</strong>
                        </td>

                        <div class="form-group">
                            Tên loại tin
                            <input class="form-control" name="Ten" type="<?php echo $row_Theloai["Ten"]; ?>"
                                   value="<?php echo $row_Theloai["Ten"]; ?>" size="40">
                            <?php
                            if (isset($error) && in_array('Ten', $error)) {
                                echo "<p class='message'>Ten không hợp lệ</p>";
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            Thứ tự
                            <input class="form-control" name="ThuTu" type="<?php echo $row_Theloai["ThuTu"]; ?>"
                                   value="<?php echo $row_Theloai["ThuTu"]; ?>" size="40">
                            <?php
                            if (isset($error) && in_array('ThuTu', $error)) {
                                echo "<p class='message'>ThuTu không hợp lệ</p>";
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            Trạng thái
                            <input name="AnHien"
                                <?php if ($row_Theloai["AnHien"] == 0) echo "checked='checked'" ?> value="0"
                                   type="radio"> Ẩn
                            <input name="AnHien"
                                <?php if ($row_Theloai["AnHien"] == 1) echo "checked='checked'" ?> value="1"
                                   type="radio">Hiện
                        </div>


                        <div class="form-group">

                            <input class="form-control btn btn-succcess" type="submit" value="Lưu lại" name="btnSua">
                            <input class="form-control btn btn-danger" type="reset" value="Reset">
                        </div>
                </table>
            </form>


        </div>


    </div>

    <footer class="footer" style="background: #f4f3ef;">
        <div class="container-fluid">
            <nav class="pull-left">

            </nav>
            <div style="margin-bottom: 12px;" class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script>
                , made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Tim</a>
            </div>
        </div>
    </footer>
</div>

</body>


</html>
