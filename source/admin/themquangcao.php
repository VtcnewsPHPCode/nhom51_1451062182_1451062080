<style type="text/css">
    .message {
        color: red;
    }
</style>
<style type="text/css">
    .message {
        color: red;
    }
</style>
<?php
require "../lib/trangquantri.php";

?>

<?php

if (isset($_POST["btnThem"])) {
    $error = array();
    if (empty($_POST['vitri'])) {
        $error[] = 'vitri';
    } else {
        $vitri = $_POST["vitri"];
        settype($vitri, "int");
    }

    if (empty($_POST['MoTa'])) {
        $error[] = 'MoTa';
    } else {
        $MoTa = $_POST["MoTa"];
    }

    if (empty($_POST['SoLanClick'])) {
        $error[] = 'SoLanClick';
    } else {
        $SoLanClick = $_POST["SoLanClick"];
        settype($SoLanClick, "int");
    }

    if (empty($_POST['Url'])) {
        $error[] = 'Url';
    } else {
        $Url = $_POST["Url"];
    }


    if ($_FILES['urlHinh']['name'] == null) {
        $error[] = 'urlHinh';
    } else {
        if ($_FILES['urlHinh']['type'] == "image/jpeg"
            || $_FILES['urlHinh']['type'] == "image/png"
            || $_FILES['urlHinh']['type'] == "image/jpg" && ($_FILES['urlHinh']['size'] < 1000000)
        ) {
            $urlHinh = $_FILES['urlHinh']['name'];
            $link_img = 'upload/quangcao/' . $urlHinh;
            // $image1 = file_get_contents($_FILES['image']['tmp_name']);
            //  $image1 = addslashes($image1);


            move_uploaded_file($_FILES["urlHinh"]["tmp_name"], "../upload/quangcao/" . $urlHinh);


        }
    }

    if (empty($error)) {

        $sql = "INSERT INTO quangcao VALUES (null,'$vitri','$MoTa','$Url','$urlHinh','$SoLanClick')";

        $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
        mysqli_set_charset($link, "UTF8");
        mysqli_query($link, $sql);

        header("location:../admin/dashboard.php?p=quanlyquangcao");
    }

}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


</head>
<body>

<div class="wrapper">
    <?php
    require "blockadmin/sidebar.php";
    ?>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="background: #76FF03;height:100%;">


        <div id="ja-mainbody" class="clearfix">

            <div class="title-module">Thêm mới quảng cáo</div>
            <form method="POST" name="edit_course" enctype="multipart/form-data">
                <table class="table-form-edit" align="center" bgcolor="#FFFFFF">
                    <div class="form-group">
                        <td colspan="2" align="left"><strong>Thêm mới quảng cáo</strong>
                    </div>

                    <div class="form-group">
                        Vị trí QC
                        <input class="form-control" name="vitri" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('vitri', $error)) {
                            echo "<p class='message'>vi tri không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Url
                        <input class="form-control" name="Url" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('Url', $error)) {
                            echo "<p class='message'>Url không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Số lần click
                        <input class="form-control" name="SoLanClick" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('SoLanClick', $error)) {
                            echo "<p class='message'>SoLanClick không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Mô tả QC
                        <input class="form-control" name="MoTa" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('MoTa', $error)) {
                            echo "<p class='message'>MoTa  không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        URL quảng cáo
                        <input class="form-control" name="urlHinh" type="file" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('urlHinh', $error)) {
                            echo "<p class='message'>urlHinh không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        <input class="form-control btn btn-danger" type="submit" value="Lưu lại" name="btnThem">
                        <input class="form-control btn btn-danger" type="reset" value="Reset">
                    </div>
                </table>
            </form>


        </div>


    </div>

    <footer class="footer" style="background: #f4f3ef;">
        <div class="container-fluid">
            <nav class="pull-left">

            </nav>
            <div style="margin-bottom: 12px;" class="copyright pull-right">
                &copy;
                <script>document.write(new Date().getFullYear())</script>
                , made with <i class="fa fa-heart heart"></i> by <a
                        href="http://localhost/nhom51_1451062182_1451062080/source/index.php">Creative Team
                    51_1451062182_1451062080</a>

            </div>
        </div>
    </footer>
</div>
</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="assets/js/paper-dashboard.js"></script>

<!-- Paper Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>


</html>
