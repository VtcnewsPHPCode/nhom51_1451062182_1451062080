<?php
ob_start();
session_start();
/*if (!isset($_SESSION["idUser"]) && $_SESSION["idGroup"] != 1) {
    header("location:../index.php");

}*/
?>

<!-- empty thoat -->
<?php
if (isset($_POST["btnExit"])) {
    unset($_SESSION["idUser"]);
    unset($_SESSION["Username"]);
    unset($_SESSION["HoTen"]);
    unset($_SESSION["idGroup"]);
    header("location:formLogin.php");

}
?>
<?php
if (isset($_GET["p"])) {
    $p = $_GET["p"];
} else
    $p = "";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">


        <div class="sidebar-wrapper">

            <?php
            require "blockadmin/sidebar.php";
            ?>
        </div>
    </div>

    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="background: #76FF03;height:100%;">


        <?php
        switch ($p) {

            case "quanlyuser":
                require "listuser.php";
                break;
            case "quanlytheloai":
                require "listTheLoai.php";
                break;
            case "quanlytin":
                require "listtin.php";
                break;
            case "quanlyquangcao":
                require "listquangcao.php";
                break;
            case "quanlyloaitin":
                require "listloaitin.php";
                break;
            case "quanlycomment":
                require "commentlist.php";
                break;
            case "doipass":
                require "doi-mat-khau.php";
                break;
            case "resetpass":
                require "reset_pass.php";
                break;
            default:
                require "listuser.php";
                break;
        }

        ?>
    </div>


</div>


</body>


</html>
