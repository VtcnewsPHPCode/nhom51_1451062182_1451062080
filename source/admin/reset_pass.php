<?php ob_start();
	if(!isset($_SESSION['idUser']))
	{
		header('Location: ../Login.php');
	}
function selectTenUser($id){
    global $link;
    $query= "SELECT Username FROM `users` WHERE idUser=$id";
    $result= mysqli_query($link,$query);
    if(mysqli_num_rows($result)==1)
    {
        $row= mysqli_fetch_assoc($result);
        return $row['Username'];
    }else return 0;
}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>VTC NEWS</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
  	<link rel="stylesheet" type="text/css" href="css-quan-tri.css">
	<meta charset="utf-8">
	<style type="text/css"> .required{color: red;}</style>

</head>
<body>
	<div  class="col-md-10" style="height:100%">
		<div style="width:100%; height:100%" class="result" >
			<?php 
				//kiểm tra xem id có phải kiếu số và được set chưa
				if( isset($_GET['idUser']) && FILTER_VAR($_GET['idUser'],FILTER_VALIDATE_INT,array('min_range'>=1)) )
				{
					$idUser=$_GET['idUser'];

				}else{
				}


				//lấy  thông tin tài khoản cần reset
				$username= selectTenUser($id);
				if($username=='0')
				{
					$message="<p class='required'>Tài khoản không tồn tại</p>";
				} 
				if($_SERVER['REQUEST_METHOD']=="POST")
				{
					
					$matkhaumoi=$_POST['matkhaumoi'];
					if(trim($_POST['matkhaumoi'])!=trim($_POST['xacnhanMKmoi']))
					{
						$message="<p class='required'>Mật khẩu mới không giống nhau</p>";
					}else{
						$query_change="UPDATE users SET Password=md5(trim('{$matkhaumoi}') )WHERE idUser=$idUser  ";
                        mysqli_set_charset($link, "UTF8");
                        $result_change=mysqli_query($link, $sql);

                        if($result_change==1)
						{
							$message="<p class='required'>Reset mật khẩu thành công</p> ";

						}else{
							$message="<p class='required'>Reset mật khẩu thất bại</p> ";
						}
					}
				}

			 ?>


			 <form name="frmDoiMatKhau" method="POST">
			 	<?php 
					if(isset($message)){
						echo $message;
						
					}
				?>
				<h3>RESET MẬT KHẨU</h3>
				<div class="form-group">
					<label>Tài khoản</label>
					<input type="text" name="taikhoan" value="<?php if($username!='0') echo $username; ?>" class="form-control" disabled="true">
				</div>

				<div class="form-group">
					<label>Mật khẩu mới</label>
					<input type=password name="matkhaumoi" value="" class="form-control">
				</div>

				<div class="form-group">
					<label>Xác nhận mật khẩu mới</label>
					<input type=password name="xacnhanMKmoi" value="" class="form-control">
				</div>
				<input type="submit" name="submit" class="btn btn-primary" value="Reset mật khẩu">
			</form>
			
		</div>
	</div>
	
	
</body>
</html>
