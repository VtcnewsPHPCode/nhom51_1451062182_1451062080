<style type="text/css">
    .message {
        color: red;
    }
</style>
<?php
require "../lib/trangquantri.php";

?>
<?php
$idTin = $_GET["idTin"];
settype($idTin, "int");
$row_Theloai = ChiTietTin($idTin);

?>
<?php

if (isset($_POST["btnSua"])) {
    $error = array();
    if (empty($_POST['TieuDe'])) {
        $error[] = 'TieuDe';
    } else {
        $TieuDe = $_POST["TieuDe"];
        $TieuDe_KhongDau = changeTitle($TieuDe);
    }

    if (empty($_POST['TomTat'])) {
        $error[] = 'TomTat';
    } else {
        $TomTat = $_POST["TomTat"];
    }


    if (empty($_POST['Ngay'])) {
        $error[] = 'Ngay';
    } else {
        $Ngay = $_POST["Ngay"];
    }

    if (empty($_POST['idUser'])) {
        $error[] = 'idUser';
    } else {
        $idUser = $_POST["idUser"];
        settype($idUser, "int");
    }


    if (empty($_POST['Content'])) {
        $error[] = 'Content';
    } else {
        $Content = $_POST["Content"];
    }

    if (empty($_POST['idLT'])) {
        $error[] = 'idLT';
    } else {
        $idLT = $_POST["idLT"];
        settype($idLT, "int");
    }


    if (empty($_POST['idTL'])) {
        $error[] = 'idTL';
    } else {
        $idTL = $_POST["idTL"];
        settype($idTL, "int");
    }
    if (empty($_POST['SoLanXem'])) {
        $error[] = 'SoLanXem';
    } else {
        $SoLanXem = $_POST["SoLanXem"];
        settype($SoLanXem, "int");
    }

    if (empty($_POST['TinNoiBat'])) {
        $TinNoiBat = '0';
    } else {

        $TinNoiBat = $_POST["TinNoiBat"];
    }
    if (empty($_POST['AnHien'])) {
        $AnHien = "0";
    } else {
        $AnHien = $_POST["AnHien"];
        settype($AnHien, "int");
    }
    if($_FILES['urlHinh']['size'] == null)
        $urlHinh = $_POST["anh_hi"];

    else{
        $urlHinh = $_FILES['urlHinh']['name'];
        $link_img = 'upload/tintuc/' . $urlHinh;
        move_uploaded_file($_FILES["urlHinh"]["tmp_name"], "../upload/tintuc/" . $urlHinh);


    }



     if (empty($error)) {


         /* $sql = "SELECT urlHinh FROM tin
                WHERE idTin='$idTin'
                ";
          $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
          mysqli_query($link, $sql);
          $query_img = mysqli_query($link, $sql);
          $anh_info = mysqli_fetch_assoc($query_img);*/

         $sql = "UPDATE tin SET
  TieuDe='$TieuDe',
  TieuDe_KhongDau='$TieuDe_KhongDau',
  TomTat='$TomTat',
  Ngay='$Ngay',
  idUser='$idUser',
  Content='$Content',
  idLT='$idLT',
  idTL='$idTL',
  SoLanXem='$SoLanXem',
  TinNoiBat='$TinNoiBat',
  AnHien='$AnHien'
  WHERE idTin='$idTin'
  ";
         $link = mysqli_connect('localhost', 'root', '', 'vtcnews');
         mysqli_set_charset($link, "UTF8");
         mysqli_query($link, $sql);
         header("location:../admin/dashboard.php?p=quanlytin");
     }
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>


    <script src="ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="ckfinder/ckfinder.js" type="text/javascript"></script>

</head>
<body>

<div class="wrapper">
    <?php
    require "blockadmin/sidebar.php";
    ?>
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="height:100%;">


        <div id="ja-mainbody" class="clearfix">

            <div class="title-module">Sửa tin</div>
            <form method="POST" name="edit_course" enctype="multipart/form-data">
                <table class="table-form-edit" align="center" bgcolor="#FFFFFF">
                    <div class="form-group">
                        <td colspan="2" align="left"><strong>Thêm mới tin</strong>
                    </div>

                    <div class="form-group">
                        Tiêu đề
                        <textarea class="form-control" name="TieuDe" id="TieuDe" cols="45"
                                  rows="2"><?php echo $row_Theloai["TieuDe"]; ?></textarea>
                        <?php
                        if (isset($error) && in_array('TieuDe', $error)) {
                            echo "<p class='message'>TieuDe không hợp lệ</p>";
                        }
                        ?>

                    </div>
                    <div class="form-group">
                        Tóm tắt

                        <textarea class="form-control" name="TomTat" id="TomTat" cols="45"
                                  rows="5"><?php echo $row_Theloai["TomTat"]; ?></textarea>
                        <?php
                        if (isset($error) && in_array('TomTat', $error)) {
                            echo "<p class='message'>TomTat không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        url hình
                        <input class="form-control" name="urlHinh" type="hidden"
                               value="" size="80%">
                        <input type="hidden" name="anh_hi" value="<?php echo $row_Theloai["urlHinh"]; ?>"/>

                        <?php
                        if (isset($error) && in_array('urlHinh', $error)) {
                            echo "<p class='message'>urlHinh không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Ngày
                        <input class="form-control" name="Ngay" type="date" value="<?php echo $row_Theloai["Ngay"]; ?>"
                               size="80%">
                        <?php
                        if (isset($error) && in_array('Ngay', $error)) {
                            echo "<p class='message'>Ngay không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        idUser
                        <input class="form-control" name="idUser" type="text"
                               value="<?php echo $row_Theloai["idUser"]; ?>" size="80%">
                        <?php
                        if (isset($error) && in_array('idUser', $error)) {
                            echo "<p class='message'>idUser không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Nội dung
                        <textarea class="form-control" name="Content" id="Content" cols="45"
                                  rows="5"><?php echo $row_Theloai["Content"]; ?></textarea>
                        <script type="text/javascript">
                            var editor = CKEDITOR.replace('Content', {
                                uiColor: '#9AB8F3',
                                language: 'vi',
                                skin: 'v2',
                                filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?Type=Images',
                                filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?Type=Flash',
                                filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',


                                toolbar: [
                                    ['Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
                                    ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print'],
                                    ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
                                    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
                                    ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                                    ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                                    ['Link', 'Unlink', 'Anchor'],
                                    ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                                    ['Styles', 'Format', 'Font', 'FontSize'],
                                    ['TextColor', 'BGColor'],
                                    ['Maximize', 'ShowBlocks', '-', 'About']
                                ]
                            });
                        </script>
                        <?php
                        if (isset($error) && in_array('Content', $error)) {
                            echo "<p class='message'>TomTat không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        ID Loại tin

                        <select class="form-control" name="idLT">
                            <?php
                            $theloai = DanhSachLoaiTin();
                            while ($row_theloai = mysqli_fetch_assoc($theloai)) {
                                ?>
                                <option value="<?php echo $row_theloai['idLT']; ?> "><?php echo $row_theloai['Ten']; ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <div class="form-group">
                        ID Thể loại

                        <select class="form-control" name="idTL">
                            <?php
                            $theloai = DanhSachTheLoai();
                            while ($row_theloai = mysqli_fetch_assoc($theloai)) {
                                ?>
                                <option value="<?php echo $row_theloai['idTL']; ?> "><?php echo $row_theloai['TenTL']; ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <div class="form-group">
                        Số Lần Xem
                        <input class="form-control" name="SoLanXem" type="text" value="" size="80%">
                        <?php
                        if (isset($error) && in_array('SoLanXem', $error)) {
                            echo "<p class='message'>SoLanXem không hợp lệ</p>";
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        Tin nổi bật
                        <input name="TinNoiBat" value="0" checked=checked type="radio"> Tin bình thường
                        <input name="TinNoiBat" value="1" type="radio">Tin nổi bật
                        <?php
                        if (isset($error) && in_array('TinNoiBat', $error)) {
                            echo "<p class='message'>TinNoiBat không hợp lệ</p>";
                        }
                        ?>
                    </div>

                    <div class="form-group">

                        Ẩn Hiện


                        <input name="AnHien" value="0" checked=checked type="radio"> Ẩn
                        <input name="AnHien" value="1" type="radio">Hiện

                    </div>

                    <div class="form-group">

                        <input class="form-control btn btn-success" type="submit" value="Lưu lại" name="btnSua">
                        <input class="form-control btn btn-danger" type="reset" value="Reset">
                    </div>
                </table>
            </form>


        </div>


    </div>




</div>
</body>


</html>