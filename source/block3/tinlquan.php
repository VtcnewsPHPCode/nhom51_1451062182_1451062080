<?php
if (isset($_GET["idLT"])) {
    $idLT = $_GET["idLT"];
    settype($idLT, "int");
} else {
    $idLT = 1;
}
if (isset($_GET["trang"]) && $_GET["trang"] != 0) {
    $trang = $_GET["trang"];
    settype($trang, "int");

} else {
    $trang = 1;
}

?>

<ul class="main-21">
    <?php
    $sotin1trang = 10;
    $from = ($trang - 1) * $sotin1trang;
    $tin = TinTheoLoai_PhanTrang($idLT, $from, $sotin1trang);
    while ($row_tin = mysqli_fetch_array($tin)) {
        ?>
        <li>
            <div class="row">
                <div class="col-md-4"><a
                            href="index2.php?idLT=<?php echo $row_tin['idLT'] ?>&idTin=<?php echo $row_tin['idTin']; ?>&trang=1"
                            class="" title="<?php echo $row_tin['TieuDe']; ?>"><img class=""
                                                                                    src="upload/tintuc/<?php echo $row_tin['urlHinh']; ?>"
                                                                                    width="160"
                                                                                    height="110"
                                                                                    alt="<?php echo $row_tin['TieuDe']; ?>"/></a>
                </div>
                <div class="col-md-8">
                    <h2 class="title"><a
                                href="index2.php?idLT=<?php echo $row_tin['idLT'] ?>&idTin=<?php echo $row_tin['idTin']; ?>&trang=1"
                                class="" title="<?php echo $row_tin['TieuDe']; ?>"><?php echo $row_tin['TieuDe']; ?></a>
                    </h2 class="title">
                    <p class="des"><?php echo $row_tin['TomTat']; ?></p>
                </div>
            </div>
        </li>
        <?php
    }
    ?>
</ul>
<div class="paging" style="text-align: center">
    <?php
    $t = TinTheoLoai($idLT);
    $tongsotin = mysqli_num_rows($t);
    $tongsotrang = ceil($tongsotin / $sotin1trang);
    for ($i = 1; $i < $tongsotrang; $i++) {
        ?>
        <a href="index3.php?idLT=<?php echo $idLT; ?>&trang=<?php echo $i; ?>&trang=1"><?php echo $i; ?></a>

        <?php
    }
    ?>


</div>
