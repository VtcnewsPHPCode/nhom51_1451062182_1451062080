<?php
require "lib/chitiettin.php";

if (isset($_GET["idLT"])) {
    $idTin = $_GET["idLT"];
    settype($idLT, "int");
} else {
    $idLT = 1;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=1150, initial-scale=1, maximum-scale=1"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<meta http-equiv="REFRESH" content="1800"/>

<link rel="shortcut icon" href="/favicon.ico"/>
<meta http-equiv="content-language" content="vi"/>
<meta name="google-site-verification" content="95CLxHep83BeIqs-9KCvB4wNT5zQ-uGd1IE_xBfTSJ0"/>
<meta name="alexaVerifyID" content="IyOAWrtFdNQSVZO_gDBWnsEy8n8"/>
<meta name="AUTHOR" content="Pháp Luật Plus"/>
<meta name="COPYRIGHT" content="Pháp Luật Plus"/>

<meta property="fb:pages" content="931166463593183"/>
<meta property="fb:app_id" content="1076004079115399"/>

<meta name="eclick_verify" content="dFFUXlYXLRcYGVlEahMECBMHMAIYGQ8eNk0aBw=="/>

<title>VTC News: Tin tức 24h trong ngày - Đọc báo điện tử mới nhất hôm nay</title>
<link href="css/main.css" rel="stylesheet" type="text/css"/>
<link href="css/exp.css" rel="stylesheet" type="text/css"/>

<link rel="stylesheet" type="text/css" href="/lib/explus/explus.css?v=0"/>
<title>Chính trị - Xã hội | Pháp Luật Plus</title>
<meta name="description" content="Chính trị - Xã hội"/>
<meta name="keywords" content="Chính trị - Xã hội"/>

<link rel="stylesheet" href="admin/assets/css/bootstrap.min.css" type="text/css" media="all"/>
<link rel="stylesheet" type="text/css" href="css/index2.css"/>
<link rel="stylesheet" href="css/font-awesome.min.css">

</head>
<body>
<header>
</header>
<main>

    <?php
    require "block/menu_left.php";
    ?>
    <!-- /#cssmemu -->
    <div class="wrapper">
        <div id="header-sticky-wrapper" class="sticky-wrapper">
            <div id="header">
                <?php
                require "block/header_menu.php";
                ?>
            </div>
        </div>
        <div class="news_hot pkg">

            <?php
            require "block/maraquee_hot.php";
            ?>

            <ul class="fr list_weather">
                <li style="float: right;"><a title="Sự kiện nóng trong ngày" href="index3.php?idLT=2">SỰ KIỆN HOT</a></li>
                <li style="float: right; margin: 0;">HOTLINE: MB - 01255911911, MN - 0911848186</li>
            </ul>

            <div style="clear: both;"></div>

        </div>


        <div class="inner" style="background-color: white; padding: 0 10px 10px 10px;">
            <div class="top-main">
                <div class="pull-left" style="display: inline-block; width: calc(100% - 300px);">
                    <div class="breakcrumb">
                        <div class="pull-left">
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="pull-right" style="display: inline-block; width: 300px;">
                    <div class="icons fanpages">
                        <a class="fb" href="https://www.facebook.com/" rel="nofollow" target="_blank" title="Facebook">Facebook</a>
                        <a class="gg" href="https://plus.google.com/" rel="nofollow" target="_blank" title="Google">Google</a>
                        <a class="tt" href="https://twitter.com/" rel="nofollow" target="_blank"
                           title="Twitter">Twitter</a>
                        <a class="yt" href="https://www.youtube.com/" rel="nofollow" target="_blank" title="Youtube">Youtube</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="main-1" style="border-bottom: 1px solid #e2e2e3;padding-bottom: 13px;margin-bottom: 13px;">
                <div class="row pull-left" style="width: calc(100% - 300px);">
                    <?php
                    require "block3/top_tinchitiet.php";
                    ?>


                </div>


                <div class="col-fixed-300 pull-right">
                    <style>
                        .bx-controls {
                            text-align: center;
                            margin-top: 5px;
                        }

                        .bx-controls .bx-pager .bx-pager-item {
                            display: inline-block;
                        }

                        .bx-controls .bx-pager .bx-pager-item a {
                            display: inline-block;
                            margin: 0 4px;
                            width: 11px;
                            height: 11px;
                            background: #EEE;
                            border-radius: 11px;
                            border: 1px solid #BABABA;
                            text-indent: -9999px;
                        }

                        .bx-controls .bx-pager .bx-pager-item a.active {
                            background: #000;
                            border: 1px solid #000;
                        }
                    </style>
                    <p style="border: 1px solid #e2e2e3;border-bottom: none;color: #405f9d;border-top: 1px solid #405f9d;padding: 5px 9px 3px;font-size: 12px;text-transform: uppercase;margin: 0;">
                        <a href="index3.php?idLT=5"><b>Video xem nhiều</b></a></p>


                    <div class="main-13" style="margin-top: 0; margin-bottom: 13px; padding: 8px;">
                        <?php
                        require "block3/videoxem.php";
                        ?>
                    </div>


                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <?php
                    require "block3/tinlquan.php";
                    ?>


                </div>
                <div class="col-md-6">
                    <div class="col-fixed-160 pul-left">
                        <div style="text-align:center;margin-top:0px;margin-bottom:0px;">

                        </div>
                        <div class="related-letter" style="margin-top: 23px;"><a
                                    href="index3.php?idLT=1">MULTIMEDIA PLUS</a></div>

                        <?php
                        require "block2/multi.php";
                        ?>


                    </div>
                    <div class="col-fixed-300 pull-right">
                        <style>
                            .box-paper {
                                position: relative;
                            }

                            .box-paper .bx-controls .bx-prev {
                                display: inline-block;
                                width: 24px;
                                height: 30px;
                                position: absolute;
                                left: 0;
                                top: 34%;
                                background: url('/templates/themes/images/slide-button-trans.png') no-repeat;
                            }

                            .box-paper .bx-controls .bx-next {
                                display: inline-block;
                                width: 24px;
                                height: 30px;
                                position: absolute;
                                right: 0;
                                top: 34%;
                                background: url('/templates/themes/images/slide-button-trans.png') -14px 0 no-repeat;
                            }
                        </style>
                        <div class="box-paper" style="margin-top: 0; padding-top: 18px;">


                            <?php
                            require "block2/qcao_left_2.php";
                            ?>
                        </div>
                        <?php
                        require "block3/qcao_left3.php";
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <style>
                #list-ttdn {
                    margin-top: 13px;
                }

                .ttdn .bx-wrapper {
                    padding: 0 30px;
                }

                .ttdn .bx-viewport {
                    min-height: 200px;
                }

                .ttdn .bx-controls .bx-prev {
                    display: inline-block;
                    width: 24px;
                    height: 30px;
                    position: absolute;
                    left: 0;
                    top: 31%;
                }

                .ttdn .bx-controls .bx-next {
                    display: inline-block;
                    width: 24px;
                    height: 30px;
                    position: absolute;
                    right: 0;
                    top: 31%;
                }

                <
                div class

                =
                "main-4 ttdn"
                id

                =
                "admStickyFooter"
                >

                <
                /
                div > <

                /
                div >
                <

                /
                main >
                < div class

                =
                "scroll-top-wrapper "
                >
                <
                span class

                =
                "scroll-top-inner"
                >
                <
                i class

                =
                "fa fa-2x fa-arrow-circle-up"
                >
                <
                /
                i >
                <

                /
                span >
                <

                /
                div >
                < style type

                =
                "text/css"
                >
                .scroll-top-wrapper {
                    position: fixed;
                    opacity: 0;
                    visibility: hidden;
                    overflow: hidden;
                    text-align: center;
                    z-index: 99999999;
                    background-color: rgba(64, 95, 157, 0.6);
                    color: #eeeeee;
                    width: 50px;
                    height: 48px;
                    line-height: 48px;
                    right: 30px;
                    bottom: 30px;
                    padding-top: 2px;
                    border-top-left-radius: 10px;
                    border-top-right-radius: 10px;
                    border-bottom-right-radius: 10px;
                    border-bottom-left-radius: 10px;
                    -webkit-transition: all 0.5s ease-in-out;
                    -moz-transition: all 0.5s ease-in-out;
                    -ms-transition: all 0.5s ease-in-out;
                    -o-transition: all 0.5s ease-in-out;
                    transition: all 0.5s ease-in-out;
                }

                .scroll-top-wrapper:hover {
                    background-color: #888888;
                }

                .scroll-top-wrapper.show {
                    visibility: visible;
                    cursor: pointer;
                    opacity: 1.0;
                }

                .scroll-top-wrapper i.fa {
                    line-height: inherit;
                }
            </style>
            <?php
            require "block/footer.php";
            ?>
</body>


</html>