<style>
    .new-com-cnt {
        padding-top: 10px;
        margin-bottom: 10px;
        border-top: #d9d9d9 1px dashed;
    }

    user agent stylesheet
    div {
        display: block;
    }
    Inherited from body
    style.css:1
    body {
        background: transparent;
        font: 12px Arial, sans-serif;
        margin: 0;
        line-height: 18px;
    }
</style>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>New issue - A005 - Karaoke - Sound OF Soul - Liz</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="Redmine"/>
    <meta name="keywords" content="issue,bug,tracker"/>
    <meta name="csrf-param" content="authenticity_token"/>
    <meta name="csrf-token"
          content="2fr5yswak4pouRF4SaUkxnjFN9ZnYLdF4zPc2BEGDfNyfpqWwjmk1P4ryGDEF+WukGucl+Mkgm7eEESg5E4dtQ=="/>
    <link rel='shortcut icon' href='/favicon.ico'/>
    <link rel="stylesheet" media="all" href="/stylesheets/jquery/jquery-ui-1.11.0.css"/>
    <link rel="stylesheet" media="all" href="/stylesheets/application.css"/>
    <link rel="stylesheet" media="all" href="/stylesheets/responsive.css"/>

    <script src="/javascripts/jquery-1.11.1-ui-1.11.0-ujs-3.1.4.js"></script>
    <script src="/javascripts/application.js"></script>
    <script src="/javascripts/responsive.js"></script>
    <script>
        //<![CDATA[
        $(window).load(function () {
            warnLeavingUnsaved('The current page contains unsaved text that will be lost if you leave this page.');
        });
        //]]>
    </script>

    <link rel="stylesheet" media="all" href="/plugin_assets/scrum/stylesheets/scrum.css"/>
    <script src="/plugin_assets/scrum/javascripts/scrum.js"></script>
    <!-- page specific tags -->
    <script src="/javascripts/jstoolbar/jstoolbar-textile.min.js"></script>
    <script src="/javascripts/jstoolbar/lang/jstoolbar-en.js"></script>
    <link rel="stylesheet" media="screen" href="/stylesheets/jstoolbar.css"/>
    <script>
        //<![CDATA[
        var datepickerOptions = {
            dateFormat: 'yy-mm-dd',
            firstDay: 0,
            showOn: 'button',
            buttonImageOnly: true,
            buttonImage: '/images/calendar.png',
            showButtonPanel: true,
            showWeek: true,
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            beforeShow: beforeShowDatePicker
        };
        //]]>
    </script>
    <script src="/javascripts/attachments.js"></script>
    <meta name="robots" content="noindex,follow,noarchive"/>
</head>
<body class="project-karaoke-sound-of-soul controller-issues action-new">
<div id="wrapper">

    <div class="flyout-menu js-flyout-menu">


        <div class="flyout-menu__search">
            <form action="/projects/karaoke-sound-of-soul/search" accept-charset="UTF-8" method="get"><input name="utf8"
                                                                                                             type="hidden"
                                                                                                             value="&#x2713;"/>
                <input type="hidden" name="issues" value="1"/>
                <label class="search-magnifier search-magnifier--flyout" for="flyout-search">&#9906;</label>
                <input type="text" name="q" id="flyout-search" class="small js-search-input" placeholder="Search"/>
            </form>
        </div>

        <div class="flyout-menu__avatar flyout-menu__avatar--no-avatar">
            <a class="user active" href="/users/55">nganq</a>
        </div>

        <h3>Project</h3>
        <span class="js-project-menu"></span>

        <h3>General</h3>
        <span class="js-general-menu"></span>

        <span class="js-sidebar flyout-menu__sidebar"></span>

        <h3>Profile</h3>
        <span class="js-profile-menu"></span>

    </div>

    <div id="wrapper2">
        <div id="wrapper3">
            <div id="top-menu">
                <div id="account">
                    <ul>
                        <li><a class="my-account" href="/my/account">My account</a></li>
                        <li><a class="logout" rel="nofollow" data-method="post" href="/logout">Sign out</a></li>
                    </ul>
                </div>
                <div id="loggedas">Logged in as <a class="user active" href="/users/55">nganq</a></div>
            </div>

            <div id="header">

                <a href="#" class="mobile-toggle-button js-flyout-menu-toggle-button"></a>

                <h1>A005 - Karaoke - Sound OF Soul</h1>

                <div id="main-menu">
                    <ul>
                        <li><a class="overview" href="/projects/karaoke-sound-of-soul">Overview</a></li>
                        <li><a class="activity" href="/projects/karaoke-sound-of-soul/activity">Activity</a></li>
                        <li><a class="kanbans" href="/projects/karaoke-sound-of-soul/issues/kanbans">Kanbans</a></li>
                        <li><a class="issues" href="/projects/karaoke-sound-of-soul/issues">Issues</a></li>
                        <li><a accesskey="7" class="new-issue selected"
                               href="/projects/karaoke-sound-of-soul/issues/new">New issue</a></li>
                        <li><a class="gantt" href="/projects/karaoke-sound-of-soul/issues/gantt">Gantt</a></li>
                        <li><a class="work-time" href="/work_time/show/karaoke-sound-of-soul">WorkTime</a></li>
                        <li><a class="calendar" href="/projects/karaoke-sound-of-soul/issues/calendar">Calendar</a></li>
                        <li><a class="news" href="/projects/karaoke-sound-of-soul/news">News</a></li>
                        <li><a class="documents" href="/projects/karaoke-sound-of-soul/documents">Documents</a></li>
                        <li><a class="wiki" href="/projects/karaoke-sound-of-soul/wiki">Wiki</a></li>
                        <li><a class="files" href="/projects/karaoke-sound-of-soul/files">Files</a></li>
                        <li><a class="settings" href="/projects/karaoke-sound-of-soul/settings">Settings</a></li>
                    </ul>
                </div>
            </div>

            <div id="main" class="nosidebar">
                <div id="sidebar">


                </div>

                <div id="content">

                    <h2>New issue</h2>


                    <form id="issue-form" class="new_issue" enctype="multipart/form-data"
                          action="/projects/karaoke-sound-of-soul/issues" accept-charset="UTF-8" method="post"><input
                                name="utf8" type="hidden" value="&#x2713;"/><input type="hidden"
                                                                                   name="authenticity_token"
                                                                                   value="Gf12sL9QwQX69MhAFrzNnJL5+W5wvU9YjhAwMAhC8SmyeRXssXP2W2xmEVibDgz0eldSL/T5enOzM6hI/Qrhbw=="/>


                        <div class="box tabular">
                            <div id="all_attributes">


                                <input type="hidden" name="form_update_triggered_by" id="form_update_triggered_by"
                                       value=""/>


                                <p><label for="issue_tracker_id">Tracker<span class="required"> *</span></label><select
                                            onchange="updateIssueFrom(&#39;/projects/karaoke-sound-of-soul/issues/new.js&#39;, this)"
                                            name="issue[tracker_id]" id="issue_tracker_id">
                                        <option selected="selected" value="1">Bug</option>
                                        <option value="2">Feature</option>
                                        <option value="3">Support</option>
                                    </select></p>

                                <p><label for="issue_subject">Subject<span class="required"> *</span></label><input
                                            size="80" maxlength="255" type="text" value="" name="issue[subject]"
                                            id="issue_subject"/></p>

                                <p>
                                    <label for="issue_description">Description</label>

                                    <span id="issue_description_and_toolbar">
    <textarea cols="60" rows="10" accesskey="e" class="wiki-edit" name="issue[description]" id="issue_description">
</textarea>
</span></p>
                                <script>
                                    //<![CDATA[
                                    var wikiToolbar = new jsToolBar(document.getElementById('issue_description'));
                                    wikiToolbar.setHelpLink('/help/en/wiki_syntax_textile.html');
                                    wikiToolbar.draw();
                                    //]]>
                                </script>

                                <div id="attributes" class="attributes">
                                    <div class="cmt-container">

                                        <div id="line_end_list"></div>

                                        <div class="alert alert-block alert-success fade in hide" id="alert-send-ok">
                                            <button type="button" class="close" data-dismiss="alert"></button>
                                            Gửi bình luận thành công. Báo điện tử VTC News xin cảm ơn bạn.
                                        </div>

                                        <div class="new-com-bt" style="display: none;">
                                            <span>Xin vui lòng viết bằng tiếng Việt CÓ DẤU</span>
                                        </div>
                                        <div class="new-com-cnt" id="new-com-cnt" style="">
                                            <input class="jsch" type="text" id="name-com" name="name-com" value="" placeholder="Họ tên" maxlength="18">
                                            <input class="jsch" type="text" id="mail-com" name="mail-com" value="" placeholder="Địa chỉ email">
                                            <textarea class="the-new-com jsch" id="the-new-com" maxlength="1000" style="margin-bottom: 0;"></textarea>
                                            <div style="display: inline-block;">
                                                <input class="jsch" type="text" id="mail-capt" name="norobot" value="" placeholder="Mã captcha" style="float: left; margin-right: 8px;">
                                                <img class="img-captcha" src="/captcha/captcha.php" height="32">
                                            </div>
                                            <div style="clear: both;"></div>
                                            <div class="bt-add-com">Gửi bình luận</div>
                                            <div class="bt-cancel-com">Hủy</div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <!--
                                    <div class="splitcontent">
                                    <div class="splitcontentleft">
                                    <p><label for="issue_status_id">Status<span class="required"> *</span></label><select onchange="updateIssueFrom(&#39;/projects/karaoke-sound-of-soul/issues/new.js&#39;, this)" name="issue[status_id]" id="issue_status_id"><option selected="selected" value="1">New</option></select></p>
                                    <input type="hidden" name="was_default_status" value="1" />

                                    <p><label for="issue_priority_id">Priority<span class="required"> *</span></label><select name="issue[priority_id]" id="issue_priority_id"><option value="1">Low</option>
                                    <option selected="selected" value="2">Normal</option>
                                    <option value="3">High</option>
                                    <option value="4">Urgent</option>
                                    <option value="5">Immediate</option></select></p>

                                    <p><label for="issue_assigned_to_id">Assignee</label><select name="issue[assigned_to_id]" id="issue_assigned_to_id"><option value="">&nbsp;</option>
                                    <option value="55">&lt;&lt; me &gt;&gt;</option><option value="43">Kien Hoang</option><option value="55">Nga Nguyen Quynh</option><option value="49">Nghi Nguyen</option><option value="44">Thanh Thủy Nguyễn Thị</option></select></p>


                                    </div>

                                    <div class="splitcontentright">
                                    <p id="parent_issue"><label for="issue_parent_issue_id">Parent task</label><input size="10" type="text" name="issue[parent_issue_id]" id="issue_parent_issue_id" /></p>
                                    <script>
                                    //<![CDATA[
                                    observeAutocompleteField('issue_parent_issue_id', '/issues/auto_complete?project_id=karaoke-sound-of-soul&scope=tree')
                                    //]]>
                                    </script>

                                    <p id="start_date_area">
                                      <label for="issue_start_date">Start date</label><input size="10" type="text" value="2017-06-22" name="issue[start_date]" id="issue_start_date" />
                                      <script>
                                    //<![CDATA[
                                    $(function() { $('#issue_start_date').addClass('date').datepicker(datepickerOptions); });
                                    //]]>
                                    </script>
                                    </p>

                                    <p id="due_date_area">
                                      <label for="issue_due_date">Due date</label><input size="10" type="text" name="issue[due_date]" id="issue_due_date" />
                                      <script>
                                    //<![CDATA[
                                    $(function() { $('#issue_due_date').addClass('date').datepicker(datepickerOptions); });
                                    //]]>
                                    </script>
                                    </p>

                                    <p><label for="issue_estimated_hours">Estimated time</label><input size="3" type="text" name="issue[estimated_hours]" id="issue_estimated_hours" /> Hours</p>

                                    <p><label for="issue_done_ratio">% Done</label><select name="issue[done_ratio]" id="issue_done_ratio"><option selected="selected" value="0">0 %</option>
                                    <option value="10">10 %</option>
                                    <option value="20">20 %</option>
                                    <option value="30">30 %</option>
                                    <option value="40">40 %</option>
                                    <option value="50">50 %</option>
                                    <option value="60">60 %</option>
                                    <option value="70">70 %</option>
                                    <option value="80">80 %</option>
                                    <option value="90">90 %</option>
                                    <option value="100">100 %</option></select></p>
                                    </div>
                                    </div>

                                     -->

                                    <div id="cmt" class="box-comment" id="comment" style="width: 100%; float: left;">
                                        <div><h4 class="tll heading"><span class="fa fa-comments-o"></span>Bình luận
                                            </h4></div>
                                        <div class="box-comment-wrap">
                                            <form id="cmb" method="POST" name="cmb" action="">
                                                <input type="text" id="name-com" name="name-com" value=""
                                                       placeholder="Họ tên" maxlength="18">
                                                <input type="text" id="mail-com" name="mail-com" value=""
                                                       placeholder="Địa chỉ email">
                                                <textarea rows="4" id="message-com" maxlength="1000"
                                                          placeholder="Xin vui lòng viết bằng tiếng Việt CÓ DẤU"></textarea>
                                                <input type="submit" class="btn-sm-com" value="Gửi bình luận"/>
                                                <input type="button" class="btn-cancel-com" value="Hủy"/>
                                            </form>
                                            <div class="cmex" data-id="331476" data-numposts="8"></div>
                                        </div>
                                    </div>
                                </div>


                                <script>
                                    //<![CDATA[

                                    $(document).ready(function () {
                                        $("#issue_tracker_id, #issue_status_id").each(function () {
                                            $(this).val($(this).find("option[selected=selected]").val());
                                        });
                                    });

                                    //]]>
                                </script>
                            </div>


                            <p id="attachments_form"><label>Files</label><span id="attachments_fields">
</span>
                                <span class="add_attachment">
<input type="file" name="attachments[dummy][file]" class="file_selector" multiple="multiple"
       onchange="addInputFiles(this);" data-max-file-size="52428800"
       data-max-file-size-message="This file cannot be uploaded because it exceeds the maximum allowed file size (50 MB)"
       data-max-concurrent-uploads="2" data-upload-path="/uploads.js"
       data-description-placeholder="Optional description"/>
(Maximum size: 50 MB)
</span>

                            </p>

                        </div>

                        <input type="submit" name="commit" value="Create"/>
                        <input type="submit" name="continue" value="Create and continue"/>
                        <a href="#"
                           onclick="submitPreview(&quot;/issues/preview/new/karaoke-sound-of-soul&quot;, &quot;issue-form&quot;, &quot;preview&quot;); return false;"
                           accesskey="r">Preview</a>
                    </form>
                    <div id="preview" class="wiki"></div>


                    <div style="clear:both;"></div>
                </div>
            </div>
        </div>

        <div id="ajax-indicator" style="display:none;"><span>Loading...</span></div>
        <div id="ajax-modal" style="display:none;"></div>

        <div id="footer">
            <div class="bgl">
                <div class="bgr">
                    Powered by <a href="https://www.redmine.org/">Redmine</a> &copy; 2006-2017 Jean-Philippe Lang
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
